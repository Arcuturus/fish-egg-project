import matplotlib.pyplot as plt
import cv2
import glob
from PIL import Image
import re
import random
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, confusion_matrix
from resizeimage import resizeimage


# Testing machine learning algorithms on our thresholding images


# Our data
data = []
labels = []
shapes = []

# Read in images
# Labeled 1 for 4 celled divisioned eggs
# and  0 for unfertilized eggs
for filename in glob.glob('thresholds/*.png'):
    im = cv2.imread(filename, 0)
    im = im.astype(float) / 255
    im = cv2.resize(im, (290, 290))
    data.append(im)
    data.append(np.fliplr(im))
    data.append(np.flipud(im))
    if re.match("thresholds\/4cells_threshold\d+.png", filename):
        labels.append(1)
        labels.append(1)
        labels.append(1)
    else:
        labels.append(0)
        labels.append(0)
        labels.append(0)

# Get number of images in our dataset
n_samples = len(data)


# Make it to an array so we can reshape it
data = np.asarray(data)

# Reshape it to [number of samples, image[i]]
data = data.reshape(n_samples, -1)


# Split our data into training data and test data, with a test size containing 20 % of the data
X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.2)


print("Number of test data: {0}".format(len(y_test)))
classifier = KNeighborsClassifier(n_neighbors=6, metric="minkowski", p=2)

from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y =  y_train, cv = 10)
print("K-NN k-Fold:\n {}".format(accuracies))
print("Mean: {}".format(accuracies.mean()))
print("Std: {}".format(accuracies.std()))
classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)


#print("Predicted : {0}".format(y_pred))
#print("Truth: {0}".format(y_test))

cm = confusion_matrix(y_test, y_pred)
print("k-NN\n {}".format(cm))


################ SVM ####################

from sklearn import svm
classifier = svm.SVC(kernel='linear')

################ k-Fold Cross validation  ####################
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y =  y_train, cv = 10)
print("SVM: k-Fold\n {}".format(accuracies))
print("Mean: {}".format(accuracies.mean()))
print("Std: {}".format(accuracies.std()))

classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
cm = confusion_matrix(y_test, y_pred)
print("\nSVM - linear\n {}".format(cm))


classifier = svm.SVC()

################ k-Fold Cross validation  ####################
accuracies = cross_val_score(estimator = classifier, X = X_train, y =  y_train, cv = 10)
print("SVM (nl): k-Fold\n {}".format(accuracies))
print("Mean: {}".format(accuracies.mean()))
print("Std: {}".format(accuracies.std()))

classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
cm = confusion_matrix(y_test, y_pred)
print("\nSVM - Non linear\n {}".format(cm))


################ Grid Search  ####################
#from sklearn.model_selection import GridSearchCV
#parameters = [{'C': [1, 10, 100], 'kernel': ['linear']},
#              {'C': [1, 10, 100], 'kernel': ['rbf'], 'gamma': [0.5, 0.1, 0.01, 0.001]}
#             ]
#grid_search = GridSearchCV(estimator=classifier, param_grid = parameters, scoring = 'accuracy', cv = 10)
#grid_search = grid_search.fit(X_train, y_train)
#best_accuracy = grid_search.best_score_
#best_params = grid_search.best_params_
#print("Grid Search:\n {0}, params: {1}".format(best_accuracy, best_params))


################ Random forest ####################
from sklearn.ensemble import RandomForestClassifier
classifier = RandomForestClassifier(n_estimators = 100, criterion="entropy")


################ k-Fold Cross validation  ####################
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X =  X_train, y =  y_train, cv = 10)
print("Random forest: k-Fold\n {}".format(accuracies))
print("Mean: {}".format(accuracies.mean()))
print("Std: {}".format(accuracies.std()))

classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
cm = confusion_matrix(y_test, y_pred)

print("\nRANDOM FOREST\n {0}".format(cm))


