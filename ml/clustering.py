import matplotlib.pyplot as plt
import numpy as np
from sklearn import cluster
import cv2


# Read in the image
img_to_cluster = plt.imread("eggs/4cells_00.png")
h, w, c = img_to_cluster.shape
# make it 2D
img_2d = img_to_cluster.reshape(h*w,c)

# Define how many clusters
kmeans_cluster = cluster.KMeans(n_clusters=3)

# Fit the kmeans clustering on the single image
kmeans_cluster.fit(img_2d)

# Get cluster centers & labels
cluster_centers = kmeans_cluster.cluster_centers_
cluster_labels = kmeans_cluster.labels_

# Show the clustered image
plt.imshow(cluster_centers[cluster_labels].reshape(h, w, c))
plt.show()
