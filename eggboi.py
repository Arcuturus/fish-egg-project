import cv2
import numpy as np
import matplotlib.pyplot as plt

# Matplotlib tries to import tkinter from python2....
try:
    from Tkinter import *
except ImportError:
    from tkinter import *


# Select which part of the image you want to see, in this case the egg    
def area_selector(img, from_x, to_x, from_y, to_y):
    image = np.zeros([to_x-from_x, to_y-from_y], dtype=np.uint8)
    image[0:,0:] = img[from_x:to_x, from_y:to_y]
    return image

image       = cv2.imread('4cells_09.png', 0)
image_blur  = cv2.medianBlur(image, 5)
image_color = cv2.cvtColor(image_blur, cv2.COLOR_GRAY2BGR)
image_gray  = cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)

#kernel = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))

center_kernel = np.array(([0,1,0],
                          [1,1,1],
                          [0,1,0]), np.uint8)

center_simple = np.array(([0,0,0],
                          [0,1,0],
                          [0,0,0]), np.uint8)
                          

circular_kernel = np.array(([[0,0,1,0,0],
                             [0,1,0,1,0],
                             [1,0,0,0,1],
                             [0,1,0,1,0],
                             [0,0,1,0,0]]), np.uint8)

line_kernel2 = np.array(([0,0,1,0,0],
                         [0,0,1,0,0],
                         [0,0,1,0,0],
                         [0,0,1,0,0],
                         [0,0,1,0,0]), np.uint8)

simple_line_kernel3 = np.array(([0,1,0],
                                [0,1,0],
                                [0,1,0]), np.uint8)

area = area_selector(image_gray, 400, 700, 500, 900)
#area_color = cv2.cvtColor(area, cv2.COLOR_GRAY2BGR)

a_th = cv2.adaptiveThreshold(area, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,3,-1)
a_er = cv2.dilate(a_th, center_kernel, iterations=2)
a_er = cv2.erode(a_er, center_kernel, iterations=2)
a_er = cv2.erode(a_er, center_simple, iterations=10)

a_er = cv2.cvtColor(a_er, cv2.COLOR_GRAY2BGR)
a_er = cv2.cvtColor(a_er, cv2.COLOR_BGR2GRAY)

try:
    circles = cv2.HoughCircles(area, cv2.HOUGH_GRADIENT,1,20,param1=30,param2=50,minRadius=1,maxRadius=100)
except AttributeError:
    print("Could not find any circles with given parameters")
    
circles = np.uint16(np.around(circles))
for c in circles[0,:]:
    cv2.circle(area, (c[0], c[1]), c[2], (255,255,255), 2)

vertical_stack = np.stack((area, a_er))
    
#ret, th = cv2.threshold(area, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

cv2.imshow('4 celled egg', a_er)
cv2.waitKey(0)
cv2.destroyAllWindows()
