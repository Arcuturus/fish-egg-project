import numpy as np
import cv2

import segmentation as seg

image = seg.EggSelector('4cells_09.png')
egg = image.find_egg()
kernel = np.ones((5,5), np.uint8)

ret, thr = cv2.threshold(egg, 250, 255,
                         cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

opening = cv2.morphologyEx(thr, cv2.MORPH_OPEN,kernel, iterations=1)
sure_bg = cv2.dilate(opening, kernel, iterations=2)

# Finding sure foreground area
dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,0)
ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)

cv2.imshow("Image", opening)
cv2.waitKey(0)
cv2.destroyAllWindows()
