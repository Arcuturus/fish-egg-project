import cv2
import numpy as np
import matplotlib.pyplot as plt
import glob
import re

import segmentation as seg

image = seg.EggSelector('ml/eggs/uf_19.png')
egg = image.find_egg()
counter = 0

kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
kernel2 = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
kernel3 = np.array(([0,0,0,0,0],
                    [0,0,1,0,0],
                    [0,1,1,1,0],
                    [0,0,1,0,0],
                    [0,0,0,0,0]), dtype=np.uint8)
kernel4 = cv2.getStructuringElement(cv2.MORPH_OPEN, (3,3))

kernel_left = np.array(([0,1,1,1,1],
                        [0,0,1,1,1],
                        [0,0,0,1,1],
                        [0,0,1,1,1],
                        [0,1,1,1,1]), dtype=np.uint8)

kernel_upper = np.array(([0,0,0,0,0],
                         [1,0,0,0,1],
                         [1,1,0,1,1],
                         [1,1,1,1,1],
                         [1,1,1,1,1]), dtype=np.uint8)

kernel_right = np.array(([1,1,1,1,0],
                         [1,1,1,0,0],
                         [1,1,0,0,0],
                         [1,1,1,0,0],
                         [1,1,1,1,0]), dtype=np.uint8)

kernel_lower = np.array(([1,1,1,1,1],
                         [1,1,1,1,1],
                         [1,1,0,1,1],
                         [1,0,0,0,1],
                         [0,0,0,0,0]), dtype=np.uint8)

ret, threshold1 = cv2.threshold(egg, 127, 255, cv2.THRESH_TRUNC)

threshold = cv2.adaptiveThreshold(threshold1, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                  cv2.THRESH_BINARY,13,1)
threshold = cv2.erode(threshold, kernel3, iterations=3)
threshold = cv2.dilate(threshold, kernel3, iterations=5)
threshold = cv2.dilate(threshold, kernel3, iterations=1)
#threshold = cv2.erode(threshold, kernel1, iterations=1)

print(egg.shape)

plt.figure(figsize=(62.4, 31.2))
plt.subplot(121)
plt.axis('off')
plt.subplots_adjust(hspace=0, wspace=0.01, top=1, bottom=0, left=0, right=1)
#plt.savefig('inpainting.pdf', bboxinches='tight')
plt.imshow(egg, cmap='gray')
plt.subplot(122)
plt.axis('off')
plt.imshow(threshold, cmap='gray')

plt.show()

#cv2.imshow('Image', threshold)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
