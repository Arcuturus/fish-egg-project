import matplotlib.pyplot as plt
import cv2
import segmentation as seg

original = seg.EggSelector('ml/eggs/4cells_15.png')
egg = original.find_egg()
threshold = cv2.imread('ml/thresholds/4cells_threshold15.png')

size = 2

plt.figure(figsize=(6.24*size, 3.12*size))
plt.subplot(121)
plt.axis('off')
plt.subplots_adjust(hspace=0, wspace=0.01, top=1, bottom=0, left=0, right=1)
plt.imshow(egg, cmap='gray')
plt.subplot(122)
plt.axis('off')
plt.imshow(threshold, cmap='gray')
plt.savefig('thresholding.png', bboxinches='tight')

plt.show()
